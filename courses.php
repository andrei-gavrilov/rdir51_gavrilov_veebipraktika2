<?php
require_once "autoloader.php";
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Courses
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/page.css">
    <script src="bootstrap/js/bootstrap.js" >
    </script>
    <script src="js/jquery-3.2.1.min.js">
    </script>
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
        <h1 class="navbar-brand mb-0">Veebipraktika 2
        </h1>
        <div class="" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Task
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="courses.php">Courses 
                <span class="sr-only">(current)
                </span>
              </a>
            </li>
          </ul>
        </div>
      </nav>
      <div class="page-header">
        <h1>Courses
        </h1>
      </div>
    </div>
    <div class="container">
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Code
            </th>
            <th scope="col">Name
            </th>
            <th scope="col">Details
            </th>
          </tr>
        </thead>
        <tbody>
          <?php 
$db=new PDOService();
$courses=$db->getAllCourses();
foreach ($courses as $course) { ?>
          <tr>
            <td>
              <?php echo $course->getCode(); ?>
            </td>
            <td>
              <?php echo $course->getName(); ?>
            </td>
            <td>
              <a href="course.php?code=<?php echo $course->getCode();  ?>">Select
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <footer class="footer">
      <div class="container">
        <p class="text-muted">© 2017 Andrei Gavrilov, RDIR51
        </p>
      </div>
    </footer>
  </body>
</html>