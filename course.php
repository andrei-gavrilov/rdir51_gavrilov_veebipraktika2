<?php
require_once "autoloader.php";
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Course 
      <?php echo $_GET['code']?>
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/page.css">
    <script src="bootstrap/js/bootstrap.js" >
    </script>
    <script src="js/jquery-3.2.1.min.js">
    </script>
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
        <h1 class="navbar-brand mb-0">Veebipraktika 2
        </h1>
        <div class="" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Task
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="courses.php">Courses
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
    <?php
$db = new PDOService();
$course = $db->getCourseByCode($_GET['code']);
$students = $db->getStudentsFromCourse($_GET['code']); ?>
    <?php
if (empty($course))
{ ?>
    <h2>Can't find this course
    </h2>
    <?php
}
else
{ ?>
    <div class="container">
      <div class="jumbotron">
        <h2 class="display-3">
          <?php
echo $course->getCode(); ?>
        </h2>
        <p class="lead">
          <?php
echo $course->getName() . ", " . $course->getEap() . " EAP"; ?> 
        </p>
      </div>
      <?php
if (empty($students))
{ ?>
      <h2>No declared students
      </h2>
      
      <?php
}
else
{ ?>
      <h2>Students
      </h2>
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Code
            </th>
            <th scope="col">Firstname
            </th>
            <th scope="col">Lastname
            </th>
            <th scope="col">Group
            </th>
          </tr>
        </thead>
        <tbody>
          <?php
foreach($students as $student)
{ ?>
          <tr>
            <td>
              <?php
echo $student->getCode(); ?>
            </td>
            <td>
              <?php
echo $student->getFirstname(); ?>
            </td>
            <td>
              <?php
echo $student->getLastname(); ?>
            </td>
            <td>
              <?php
echo $student->getGroupCode(); ?>
            </td>
          </tr>
          <?php
} ?>
        </tbody>
      </table>
      <?php
}
 ?>
      <h4>
        <a href="courses.php">Return
        </a>
      </h4>
    </div>
    <?php

} ?>
    </div>
  <footer class="footer">
    <div class="container">
      <p class="text-muted">© 2017 Andrei Gavrilov, RDIR51
      </p>
    </div>
  </footer>
  </body>
</html>