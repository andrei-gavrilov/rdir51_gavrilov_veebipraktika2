<?php

//Интерфейс, осуществляемый классами MySQLiService.php и PDOService.php
interface IServiceDB
{
    public function connect();
    public function getAllCourses();
    public function getStudentsFromCourse($course);
    public function getCourseByCode($code);
}