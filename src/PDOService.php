<?php


class PDOService implements IServiceDB
{	
	private $connectDB;
	
	public function connect() {	
        try {
            $this->connectDB = new PDO("mysql:host=".DB_HOST.";dbname=".DB_DATABASE.";charset=".DB_CHARSET, 
                                DB_USERNAME, DB_PASSWORD);
        }		
		catch (PDOException $ex) {
			printf("Connection failed: %s", $ex->getMessage());
			exit();
		}
		return true;
	}
	public function getAllCourses() {	
			$courses=array();
			if ($this->connect()) {
				if ($result = $this->connectDB->query('SELECT * FROM course')) {
					$rows = $result->fetchAll(PDO::FETCH_ASSOC);
					foreach($rows as $row){
						$courses[]=new Course($row['code'], $row['name'], $row['eap']);
					 } 
				}
			}
			$this->connectDB=null;
			return $courses;

	}
	public function getStudentsFromCourse($course)
	{	
		$students=array();
		if ($this->connect()) {
			if ($result = $this->connectDB->prepare('SELECT s.* FROM `student` AS s 
			JOIN `declaration` AS d on s.code = d.student_code_fk
			WHERE d.course_code_fk =:course')) {
				$result->execute(array('course'=>$course));
				$rows = $result->fetchAll(PDO::FETCH_ASSOC);
				foreach($rows as $row){
					$students[]=new Student($row['code'], $row['firstname'], $row['lastname'], $row['personal_code'], $row['group_code'], $row['email']);
                 } 
			}
		}
        $this->connectDB=null;
	    return $students;	
	}
	public function getCourseByCode($code)
	{	
		$course=null;
		if ($this->connect()) {
			if ($result = $this->connectDB->prepare('SELECT * FROM course WHERE code=:code')) {
				$result->execute(array('code'=>$code));
				
				$numRows = $result->rowCount();
				if ($numRows==1) {
					$row=$result->fetch();
					$course=new Course($row['code'], $row['name'], $row['eap']);
				}
			}
		}
        $this->connectDB=null;
	    return $course;	
	}
	
}

