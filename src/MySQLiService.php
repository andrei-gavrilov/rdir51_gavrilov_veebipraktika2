<?php

class MySQLiService implements IServiceDB
{	
	private $connectDB;
	
	public function connect() {	
		$this->connectDB = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		$this->connectDB->set_charset(DB_CHARSET);
		if (mysqli_connect_errno()) {
			printf("Connection failed: %s", mysqli_connect_error());
			exit();
		}
		return true;
	}

}

