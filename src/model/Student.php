<?php

class Student{
    private  $code;
    private  $firstname;
    private  $lastname;
    private  $personal_code;
    private  $group_code;
    private  $email;

    public function __construct($code, $firstname, $lastname, $personal_code, $group_code, $email){
        $this->code = $code;
        $this->lastname = $lastname;
        $this->firstname = $firstname;
        $this->personal_code = $personal_code;
        $this->group_code = $group_code;
        $this->email = $email;
    }

    public function getCode(){
        return $this->code;
    }
    public function setCode($code){
        $this->code=$code;
    }

    public function getFirstname(){
        return $this->firstname;
    }
    public function setFirstname($firstname){
        $this->firstname=$firstname;
    }

    public function getLastname(){
        return $this->lastname;
    }
    public function setLastname($lastname){
        $this->lastname=$lastname;
    }

    public function getPersonalCode(){
        return $this->personal_code;
    }
    public function setPersonalCode($personal_code){
        $this->personal_code=$personal_code;
    }
    public function getGroupCode(){
        return $this->group_code;
    }
    public function setGroupCode($group_code){
        $this->group_code=$group_code;
    }
    public function getEmail(){
        return $this->email;
    }
    public function setEmail($email){
        $this->email=$email;
    }

}
?>