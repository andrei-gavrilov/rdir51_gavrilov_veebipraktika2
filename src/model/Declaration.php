<?php

class Declaration{
    private  $id;
    private  $course_code;
    private  $student_code;

    public function __construct($id, $course_code, $student_code){
        $this->id = $id;
        $this->course_code = $course_code;
        $this->student_code = $student_code;
    }

    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }

    public function getStudentCode(){
        return $this->student_code;
    }
    public function setStudentCode($student_code){
        $this->student_code=$student_code;
    }

    public function getCourseCode(){
        return $this->course_code;
    }
    public function setCourseCode($course_code){
        $this->course_code=$course_code;
    }

}


?>