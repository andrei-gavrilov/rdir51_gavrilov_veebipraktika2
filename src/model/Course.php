<?php

class Course{
    private  $code;
    private  $name;
    private  $eap;

    public function __construct($code, $name, $eap ){
        $this->code = $code;
        $this->name = $name;
        $this->eap = $eap;
    }

    public function getCode(){
        return $this->code;
    }
    public function setCode($code){
        $this->code=$code;
    }

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name=$name;
    }

    public function getEap(){
        return $this->eap;
    }
    public function setEap($eap){
        $this->eap=$eap;
    }

}


?>