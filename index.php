<?php
require_once "autoloader.php";
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Veebipraktika 2
    </title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/page.css">
    <script src="bootstrap/js/bootstrap.js" >
    </script>
    <script src="js/jquery-3.2.1.min.js">
    </script>
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
        <h1 class="navbar-brand mb-0">Veebipraktika 2
        </h1>
        <div class="" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Task 
                <span class="sr-only">(current)
                </span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="courses.php">Courses
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
    <div class="container">  
      <div class="row">
        <div class="col-6">
          <h1>Kolledzi tudengite registreerimine kursustele (PHP)
          </h1>
        </div>
        <div class="col-6">
          <h1>Регистрация студентов колледжа на курсы (PHP)
          </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          Loo veebirakendus, mis sisaldab informatsiooni kursuste ja registreeritud tudengitekohta
        </div>
        <div class="col-6">
          Создайте Web приложение, которое содержит информацию о курсах и студентах, которые записались на курсы
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <ul class="list-group">
            <li class="list-group-item">Pealehel ilmub kursuste nimekiri (kursuse nimetus ja kood)
            </li>
            <li class="list-group-item">Kasutaja võib valida kursuse ja vaadata teiselt lehelt kursuse täisinformatsiooni ja registreeritud tudengite nimesid.
            </li>
          </ul>
        </div>
        <div class="col-6">
          <ul class="list-group">
            <li class="list-group-item">На главной странице выводятся название курсов и их код
            </li>
            <li class="list-group-item">Пользователь может выбрать курс и посмотреть на другой странице полную информацию о курсе и кто записался на данный курс
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          Realiseerimisel looge klassid, mis kirjeldavad süsteemi mudelit
        </div>
        <div class="col-6">
          Для реализации создайте классы, которые описывают модель системы
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          Hindamise ajal hinnatakse ka kasutajaliidest
        </div>
        <div class="col-6">
          При оценивании также будет учитываться внешний вид пользовательского интерфейса
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          Kasutage Git
        </div>
        <div class="col-6">
          Используйте git при пректировании проекта
        </div>
      </div>
    </div>
    <footer class="footer">
      <div class="container">
        <p class="text-muted">© 2017 Andrei Gavrilov, RDIR51
        </p>
      </div>
    </footer>
  </body>
</html>